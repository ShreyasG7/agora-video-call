import React, { Component } from 'react';
import { uuid } from 'uuidv4';
import AgoraRTC from 'agora-rtc-sdk';

const client = AgoraRTC.createClient({ mode: 'live', codec: 'h264' });

const userId = uuid();

export default class Call extends Component {
  state = {
    remoteStreams: {},
  };

  localStream = AgoraRTC.createStream({
    streamID: userId,
    audio: true,
    video: true,
    screen: false,
  });

  componentDidMount() {
    this.initClient();
    this.initLocalStream();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.channel !== this.props.channel && this.props.channel !== '') {
      this.joinChannel();
    }
  }

  joinChannel = () => {
    client.join(
      null,
      this.props.channel,
      userId,
      (uid) => {
        console.log('User ' + uid + ' join channel successfully');
        client.publish(this.localStream, function (err) {
          console.log('Publish local stream error: ' + err);
        });

        client.on('stream-published', function (evt) {
          console.log('Publish local stream successfully');
        });
      },
      function (err) {
        console.log('Join channel failed', err);
      }
    );
  };

  initClient = () => {
    client.init(
      '6b08e5713e2d4ec3bfb241718ba24b42',
      function () {
        console.log('AgoraRTC client initialized');
      },
      function (err) {
        console.log('AgoraRTC client init failed', err);
      }
    );
    this.subscribeToClient();
  };

  initLocalStream = () => {
    this.localStream.init(
      () => {
        console.log('getUserMedia successfully');
        this.localStream.play('agora_local');
      },
      function (err) {
        console.log('getUserMedia failed', err);
      }
    );
  };

  subscribeToClient = () => {
    client.on('stream-added', this.onStreamAdded);
    client.on('stream-subscribed', this.onRemoteClientAdded);

    client.on('stream-removed', this.onStreamRemoved);

    client.on('peer-leave', this.onPeerLeave);
  };

  onStreamRemoved = (evt) => {
    let me = this;
    let stream = evt.stream;
    if (stream) {
      let streamId = stream.getId();
      let { remoteStreams } = me.state;

      stream.stop();
      delete remoteStreams[streamId];

      me.setState({ remoteStreams });

      console.log('Remote stream is removed ' + stream.getId());
    }
  };

  onPeerLeave = (evt) => {
    let me = this;
    let stream = evt.stream;
    if (stream) {
      let streamId = stream.getId();
      let { remoteStreams } = me.state;

      stream.stop();
      delete remoteStreams[streamId];

      me.setState({ remoteStreams });

      console.log('Remote stream is removed ' + stream.getId());
    }
  };

  onStreamAdded = (evt) => {
    let stream = evt.stream;
    console.log('New stream added: ' + stream.getId());
    this.setState(
      {
        remoteStreams: {
          ...this.state.remoteStreams,
          [stream.getId()]: stream,
        },
      },
      () => {
        // Subscribe after new remoteStreams state set to make sure
        // new stream dom el has been rendered for agora.io sdk to pick up
        client.subscribe(stream, function (err) {
          console.log('Subscribe stream failed', err);
        });
      }
    );
  };

  onRemoteClientAdded = (evt) => {
    let remoteStream = evt.stream;
    this.state.remoteStreams[remoteStream.getId()].play(
      'agora_remote ' + remoteStream.getId()
    );
  };

  render() {
    return (
      <div>
        <div id="agora_local" style={{ width: '400px', height: '400px' }} />
        {Object.keys(this.state.remoteStreams).map((key) => {
          let stream = this.state.remoteStreams[key];
          let streamId = stream.getId();
          return (
            <div
              key={streamId}
              id={`agora_remote ${streamId}`}
              style={{
                width: '400px',
                height: '400px',
              }}
            />
          );
        })}
      </div>
    );
  }
}
